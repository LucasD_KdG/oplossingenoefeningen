package pdt;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Student {
    private String naam;
    private KlasGroep huidigeKlasgroep;
    private List<Transfer> transfers = new ArrayList();

    public Student(KlasGroep huidigeKlasgroep, String naam) {
        this.huidigeKlasgroep = huidigeKlasgroep;
        this.naam = naam;
    }

    public KlasGroep getHuidigeKlasgroep() {
        return huidigeKlasgroep;
    }

    public void verhuisNaarGroep(KlasGroep groep){
        transfers.add(new Transfer(this, groep, LocalDate.now()));
        this.huidigeKlasgroep = groep;
    }

    public String getNaam() {
        return naam;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Student{");
        sb.append("naam='").append(naam).append('\'');
        sb.append(", huidigeKlasgroep=").append(huidigeKlasgroep);
        sb.append(", transfers=").append(transfers);
        sb.append('}');
        return sb.toString();
    }
}
