package pdt;

import java.time.LocalDate;

public class Transfer {
    private final LocalDate transferDate;
    private final Student student;
    private final KlasGroep komtVanGroep;
    private final KlasGroep gaatNaarGroep;

    public Transfer(Student student, KlasGroep gaatNaarGroep, LocalDate transferDate) {
        this.student = student;
        this.komtVanGroep = student.getHuidigeKlasgroep();
        this.gaatNaarGroep = gaatNaarGroep;
        this.transferDate = transferDate;
    }

    public Student getStudent() {
        return student;
    }

    public KlasGroep getKomtVanGroep() {
        return komtVanGroep;
    }

    public KlasGroep getGaatNaarGroep() {
        return gaatNaarGroep;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Transfer{");
        sb.append("transferDate=").append(transferDate);
        sb.append(", komtVanGroep=").append(komtVanGroep);
        sb.append(", gaatNaarGroep=").append(gaatNaarGroep);
        sb.append('}');
        return sb.toString();
    }
}