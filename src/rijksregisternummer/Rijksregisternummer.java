package rijksregisternummer;

public class Rijksregisternummer {
    private final String nummer;

    public Rijksregisternummer(String nummer) {
        if(!isValid(nummer)){
            throw new NumberFormatException("Geen geldig rijksregisternummer");
        }
        this.nummer = nummer;
    }

    public String getNummer() {

        return nummer;
    }

    private boolean isValid(String nummer){
        int part1 = 0;
        int part2= 98;

        try {
            part1 =  Integer.valueOf(nummer.substring(0,9));
            part2 =  Integer.valueOf(nummer.substring(10,11));
        }
        catch(NumberFormatException e)        {
            return false;
        }
       return (part1 % 97) == part2;
    }

}
