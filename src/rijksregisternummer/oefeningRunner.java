package rijksregisternummer;

public class oefeningRunner {
    public static void main(String[] args) {
        try {
            Rijksregisternummer juistNummer = new Rijksregisternummer("20000101008");
            System.out.println("juistNummer: " + juistNummer.getNummer());
        } catch (NumberFormatException e) {
            System.out.println("Fout opgetreden: " + e);
        }
        try {
            Rijksregisternummer foutNummer = new Rijksregisternummer("20000101009");
            System.out.println("foutNummer: " + foutNummer);
        } catch (NumberFormatException e) {
            System.out.println("Fout opgetreden: " + e);
        }
        try {
            Rijksregisternummer nummerMetTekst = new Rijksregisternummer("20O00101008");
            System.out.println("nummermetTekst: " + nummerMetTekst);
        } catch (NumberFormatException e) {
            System.out.println("Fout opgetreden: " + e);
        }


    }
}
