In dit project kan je code oplossingen vinden van een aantal in de les gemaakte oefeningen. 

Studenten hebben het vaak moeilijk met het linken van de diagrammen met code. 
Werk bij het studeren zelf de code uit op basis van de diagrammen en gebruik dit project om je eigen code te valideren.